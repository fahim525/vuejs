
 import CustomerComponent from '@/components/product/Product.vue';
 import AboutComponent from '@/components/pages/About.vue';
 import AdminDashboard from '@/components/admin/Dashboard.vue';
 import AdminProductCreate from '@/components/admin/product/Create.vue';
 import ProductShow from '@/components/product/Show.vue';

 export default[
     {path:'/',component:CustomerComponent},
     {path:'/product/:id',component:ProductShow},
     {path:'/about',component:AboutComponent},
     {path:'/admin/dashboard',component:AdminDashboard},
     {path:'/admin/product/create',component:AdminProductCreate}
 ]