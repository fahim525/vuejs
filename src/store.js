import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export const store=new Vuex.Store({
    state:{
        products:[],
        myvalue:null
    },

    getters:{
        AllProducts(state){
            return state.products
        },
        getMyValue(state)
        {
            return state.myvalue
        }
    },

    mutations:{
        updateProduct(state,dt){
            state.products=dt
        },
        updateValue(state)
        {  
            state.myvalue++
        }
    },

    actions:{
       getProducts(context){
            axios.get('http://localhost:8000/api/product')
            .then((response) => {
               context.commit('updateProduct',response.data);
            })
            .catch(function (error) {
                console.log(error);
            })
            .then(function () {
            });
       },

       incrementValue(context){
            setTimeout(function(){
                context.commit('updateValue');
            },3000)
       }
    }
})